[![coverage report](https://gitlab.com/serial-lab/serialbox/badges/master/coverage.svg?job=python3_5_unit_test)](https://gitlab.com/serial-lab/serialbox/commits/master) [![build status](https://gitlab.com/serial-lab/serialbox/badges/master/build.svg)](https://gitlab.com/serial-lab/serialbox/commits/master)

```
  ____            _       _ ____            
 / ___|  ___ _ __(_) __ _| | __ )  _____  __
 \___ \ / _ \ '__| |/ _` | |  _ \ / _ \ \/ /
  ___) |  __/ |  | | (_| | | |_) | (_) >  <
 |____/ \___|_|  |_|\__,_|_|____/ \___/_/\_\

```

SerialBox solves the non-trivial problem of generating and distributing serial number information from system to system for the use in manufacturing and supply chain environments. SerialBox was built with Marijuana and Pharmaceutical production and distribution systems in mind but can be used for any application that requires a unique serial number distribution API.

# Open, Simple, Tested and Well Documented
SerialBox is easy to install, provides a simple RESTful API for fast and clean implementation, comes with a comprehensive suite of unit tests and is fully documented.

# Get Serial

SerialBox is distributed via the code on this site under the GPLv3 license and also via the gitlab docker registry. 

## Docker Compose Project
Check out the docker compose project here:

[SerialLab serial-box-docker-compose Project](https://gitlab.com/serial-lab/serial-box-docker-compose)

## Documentation
Installation and configuration instructions can be found here:

[SerialBox Dox](https://serial-lab.gitlab.io/serialbox/installation/index.html)


